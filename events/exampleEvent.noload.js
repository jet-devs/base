module.exports = {
  name: 'exampleEvent',
  event: meta => {
    // Remove unused vars
    const msg = meta.message
    const configuration = meta.config
    const client = meta.client

    // Do something
    if (msg.content === 'ayyy') {
      msg.reply('lmao')
    }
  }
};
