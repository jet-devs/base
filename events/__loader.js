const config = require('../config.json')
const fs = require('fs')
const path = require('path')

fs.readdir(__dirname, (err, files) => {
  if (err) {
    throw err
  }

  for (let i = 0; i < files.length; i++) {
    if (files[i] !== '__loader.js') {
      if (files[i].indexOf('.noload') === -1) {
        const event = require(path.join(__dirname, files[i]));
        module.exports[files[i].substr(0, files[i].length - 3)] = event
      }
    }
  }
})
