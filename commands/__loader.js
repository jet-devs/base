const config = require('../config.json')
const fs = require('fs')
const path = require('path')

fs.readdir(__dirname, (err, files) => {
  if (err) {
    throw err
  }

  module.exports['help.message'] = [];

  for (let i = 0; i < files.length; i++) {
    if (files[i] !== '__loader.js') {
      if (files[i].indexOf('.noload') === -1) {
        const command = require(path.join(__dirname, files[i]));
        if (!command.admin) {
          const cmdHelp = {
            name: config.prefix + command.name,
            value: command.description,
            inline: true
          }

          module.exports[files[i].substr(0, files[i].length - 3)] = command
          module.exports['help.message'].push(cmdHelp)
        } else {
          module.exports[files[i].substr(0, files[i].length - 3)] = command
        }
      }
    }
  }
})
