module.exports = {
  name: 'exampleCommand',
  description: 'Template for commands',
  fullDescription: `Full description of command\nUsage: \`usage\``,
  command: meta => {
    // Remove unused vars in the new module
    const msg = meta.message
    const configuration = meta.config
    const client = meta.client

    // Do something
    msg.channel.send('pong');
  }
};
