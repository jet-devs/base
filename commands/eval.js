module.exports = {
  name: 'eval',
  description: 'Admin command',
  admin: true,
  command: meta => {
    const msg = meta.message;
    const config = meta.config;
    if (meta.config.isOwner(msg)) {
      try {
        msg.channel.send(':outbox_tray:```js\n' + eval(msg.content.substr(config.prefix.length + 5)) + '\n```')
      } catch (e) {
        msg.channel.send(':outbox_tray: :x: ```js\n' + e + '\n```')
      }
    }
  }
};
