module.exports = {
  name: 'info',
  description: 'Info about the bot',
  fullDescription: `Displays info about the bot\nUsage: \`info\``,
  command: meta => {
    const msg = meta.message
    const config = meta.config
    const client = meta.client

    msg.channel.send({
      embed: {
        title: 'Info',
        fields: [
          {
            name: 'Process Memory Usage',
            value: Math.round(process.memoryUsage().rss / (1024 * 1024)) + 'MB',
            inline: true
          },
          {
            name: 'Servers',
            value: String(client.guilds.size),
            inline: true
          },
          {
            name: 'Language',
            value: 'Node.JS ' + process.versions.node,
            inline: true
          },
          {
            name: 'Library',
            value: 'Discord.JS v' + config.package.dependencies['discord.js'],
            inline: true
          },
          {
            name: 'Framework',
            value: 'Ciasto v' + config.package['version'],
            inline: true
          },
          {
            name: 'No. of commands',
            value: Object.keys(config.commands).length - 1,
            inline: true
          }
        ],
        color: 0xFF000
      }
    });
  }
};
