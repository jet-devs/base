module.exports = {
  name: 'help',
  description: 'Displays help message',
  fullDescription: `Displays all commands or help page for specified command\nUsage: \`help\` or \`help <commandName>\``,
  command: meta => {
    const commands = require('./__loader')
    const config = meta.config
    const msg = meta.message

    const arg = meta.args[1]

    if (arg) {
      if (commands[arg]) {
        msg.channel.send('', {
          embed: {
            color: 0xFF000,
            title: commands[arg].name,
            description: commands[arg].fullDescription || commands[arg].description
          }
        })
      }
    } else {
      msg.channel.send('List of commands:', {
        embed: {
          color: 0xFF000,
          fields: commands['help.message']
        }
      })
    }
  }
}
