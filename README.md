# Base
> Modular Discord.JS bot template

This is a [Discord](https://discordapp.com) bot template based on [Discord.JS](http://github.com/hydrabolt/discord.js) used to run Jet and NanoBot.

This README only has basic info about the template.

### Configuration

Copy contents of `config.example.json` to `config.json` and fill in required fields.

### Running Base

1. Fill in required fields as stated above
2. Run `npm install`
3. Run `npm start`
