const path = require('path')
const Discord = require('discord.js')
const c = require('chalk')

const config = require('./config.json')
config.package = require('./package.json')
config.isOwner = isOwner

const client = new Discord.Client()

const commands = require('./commands/__loader')
const events = require('./events/__loader')

config.commands = commands

client.on('ready', () => {
  console.log(c.cyan('[BOT] ') + 'Spawned!')

  client.user.setGame(config.prefix + 'help')
})

client.on('message', message => {
  const args = message.content.split(' ')

  const meta = {
    args,
    client,
    config,
    message
  }

  if (message.author.bot === false) {
    let commandName = ''
    if (message.content.startsWith(config.prefix)) {
      commandName = message.content.split(' ')[0].substring(config.prefix.length)
    }

    if (commands[commandName] !== undefined) {
      commands[commandName].command(meta)
    }

    for(event in events) {
      events[event].event(meta)
    }
  }
})

function isOwner(message) {
  if (config.owner === message.author.id) {
    return true
  }
}

client.login(config.apiKeys.discord)
